from django.utils.timesince import timesince
from rest_framework import serializers
from django.urls import reverse

from accounts.api.serializers import UserDisplaySerializer
from tweets.models import Tweet


class ParentTweetModelSerializer(serializers.ModelSerializer):
    
    user = UserDisplaySerializer(read_only=True)
    tweet_url = serializers.SerializerMethodField()
    date_display = serializers.SerializerMethodField()
    timesince = serializers.SerializerMethodField()

    class Meta:
        model = Tweet
        fields = [
            'user',
            'content',
            'tweet_url',
            'timespamt',
            'date_display',
            'timesince',
        ]

    def get_tweet_url(self, obj):
        return reverse("tweet:detail", kwargs={"pk":obj.pk})

    def get_is_retweet(self, obj):
        if obj.parent:
            return True

        return False

    def get_date_display(self, obj):
        return obj.timespamt.strftime("%b %d %I:%M %p")

    def get_timesince(self, obj):
        return timesince(obj.timespamt) + " ago"


class TweetModelSerializer(serializers.ModelSerializer):
    
    user = UserDisplaySerializer(read_only=True)
    tweet_url = serializers.SerializerMethodField()
    date_display = serializers.SerializerMethodField()
    timesince = serializers.SerializerMethodField()
    parent = ParentTweetModelSerializer(read_only=True)
    retweet_url = serializers.SerializerMethodField()

    class Meta:
        model = Tweet
        fields = [
            'user',
            'content',
            'tweet_url',
            'timespamt',
            'date_display',
            'timesince',
            'parent',
            'retweet_url'
        ]

    def get_tweet_url(self, obj):
        return reverse("tweet:detail", kwargs={"pk":obj.pk})

    def get_is_retweet(self, obj):
        if obj.parent:
            return True

        return False

    def get_date_display(self, obj):
        return obj.timespamt.strftime("%b %d %I:%M %p")

    def get_timesince(self, obj):
        return timesince(obj.timespamt) + " ago"

    def get_retweet_url(self, obj):
        return reverse("tweet:retweet", kwargs={"pk":obj.pk})

