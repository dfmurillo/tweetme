from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from .models import Tweet

User = get_user_model()

class TweetModelTestCase(TestCase):
    """Test case for model"""
    def setUp(self):
        User.objects.create(username = "lkasdkljaskd")

    def test_tweet_item(self):
        obj = Tweet.objects.create(
                user = User.objects.first(),
                content="Some content"
            )
        self.assertTrue(obj.content == "Some content")
        self.assertTrue(obj.id == 1)

    def test_tweet_url(self):
        obj = Tweet.objects.create(
                user = User.objects.first(),
                content="Some content"
            )
        absolute_url = reverse("tweet:detail", kwargs={"pk": obj.pk})
        self.assertEqual(obj.get_absolute_url(), absolute_url)
