from django.contrib import admin

# Register your models here.
from .forms import TweetModelForm
from .models import Tweet

# admin.site.register(Tweet)

class TweetModelAdmin(admin.ModelAdmin):
    """docstring for TweetModelAdmin"""
    # form = TweetModelForm

    fields = ["user", "content"]
    
admin.site.register(Tweet, TweetModelAdmin)


        
            
