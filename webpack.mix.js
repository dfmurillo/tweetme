let mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let base_url = 'src/static-storage'

mix.sass('dist/scss/bootstrap.scss', base_url+'/css')
	.copy('node_modules/bootstrap-sass/assets/javascripts', base_url+'/js')
	.copy('node_modules/bootstrap-sass/assets/fonts', base_url+'/fonts')
    .sourceMaps()
